<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>
<?php do_action('wp_content_bottom'); ?>
</div>

<?php do_action('wp_body_end'); ?>
<?php $lg_option_footer_site_legal = get_option('lg_option_footer_site_legal'); ?>

<footer id="site-footer">

    <div>
        <div id="site-footer-main" class="py-3 container">
            <?php
                $footer_contact_info = get_field('footer_contact_info', 'options');
                $footer_office_hours = get_field('footer_office_hours', 'options');
            ?>
            <div class="row">
                <div class="col-md-6 col-lg-3 mb-3">
                    <div class="footer-logo mb-2">
                      <a href="/"><?php echo site_logo(); ?></a>
                    </div>
                    <?php echo $footer_contact_info; ?>
                </div>
                <div class="col-md-6 col-lg-3 mb-3">
                    <div>
                        <h2>Office Hours</h2>
                       <?php echo $footer_office_hours; ?>
                   </div>
                </div>
                <!-- <div class="col-md-6 col-lg-2 mb-3">
                    <div class="pl-lg-2">
                        <h2>Quick Links</h2>
                        <?php
                          $quickLinks = array(
                            'menu'              => 'quick-links',
                            'depth'             => 2,
                            'walker'            => new WP_Bootstrap_Navwalker()
                          );
                          wp_nav_menu($quickLinks);
                        ?>
                    </div>
                </div> -->
                <div class="col-md-6 col-lg-3 mb-3">
                    <div>
                        <h2>Services</h2>
                        <?php
                          $services = array(
                            'menu'              => 'services',
                            'depth'             => 2,
                            'walker'            => new WP_Bootstrap_Navwalker()
                          );
                          wp_nav_menu($services);
                        ?>
                    </div>
                </div>
                <div class="col-md-6 col-lg-3">
                    <div class="instagram-feed">
                        <div class="footer-instagram-ajax">
                            <!-- <?php
                            // do_action('example_ajax_request');
                            // example_ajax_request();
                            //echo do_shortcode("[instagram-feed]");
                            ?> -->
                            <?php //echo do_shortcode('[instagram-feed]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if (!$lg_option_footer_site_legal || $lg_option_footer_site_legal == 'enable'): ?>
            <div id="footer-legal" class="py-3 px-3">
                <div class="d-flex flex-column flex-md-row justify-content-md-between align-items-center flex-wrap">
                    <div class="site-info"><?php get_template_part("/templates/template-parts/footer/site-info"); ?></div>
                    <div class="site-longevity"> <?php get_template_part("/templates/template-parts/footer/site-footer-longevity"); ?> </div>
                </div>
            </div>
        <?php endif; ?>
    </div>

</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>

<?php do_action('document_end'); ?>
