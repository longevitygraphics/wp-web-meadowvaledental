<?php
/**
 * The header for our theme
 *
 */

?>

<?php do_action('document_start'); ?>

<!doctype html>
<html <?php language_attributes(); ?> <?php do_action('html_class'); ?>>
<head>
    <link rel="preload" as="font" crossorigin="anonymous" href="/wp-content/plugins/wp-plugin-lg-plugin/assets/lg-slider/fonts/slick.woff">
  <!--[if lt IE 9]>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
  <![endif]-->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
  <?php do_action('wp_header'); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

  <?php do_action('wp_body_start'); ?>

  <div id="page" class="site">
  <div class='booking-modal'>
      <div class='modal-form-wrapper card card-body'>
        <a href='#' class='exit-modal'>x</a>
        <?php 
          $modal=get_field('modal', 'option');
          echo do_shortcode($modal);
        ?>
      </div>
    </div>
  	<header id="site-header">
      <?php do_action( "lg_announcement_bar") ?>
      <div class="header-main">
        <div class="d-flex justify-content-between align-items-center flex-wrap">
          <div class="site-branding px-3">
            <div class="logo">
              <a href="/"><?php echo site_logo(); ?></a>
            </div>
            <div class="mobile-toggle"><i class="fa fa-bars" aria-hidden="true"></i></div>
          </div><!-- .site-branding -->
          <div class="d-flex flex-column justify-content-between menu-wrapper">
            <?php do_action('wp_utility_bar'); ?>
            <?php get_template_part("/templates/template-parts/header/main-nav"); ?>
          </div>
        </div>
      </div>
      <div class="mobile-header-contacts d-flex flex-wrap text-center d-lg-none">
          <a href="tel:<?php echo do_shortcode("[lg-phone-main]"); ?>" class="text-white mobile-header-phone col-6 bg-primary py-2"><i class="fas fa-phone"></i> <?php echo do_shortcode("[lg-phone-main]"); ?></a>
          <a href="mailto:<?php echo do_shortcode("[lg-email]"); ?>" class="text-white mobile-header-email col-6 py-2"><i class="far fa-envelope"></i> Email Us</a>
      </div>

    </header><!-- #masthead -->

    <div id="site-content" role="main">
      <?php do_action('wp_content_top'); ?>
