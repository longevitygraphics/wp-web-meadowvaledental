<?php

switch (get_row_layout()) {
    case 'home_page_services':
        get_template_part('/layouts/layouts/home-page-services');
        break;
    case 'home_page_team':
        get_template_part('/layouts/layouts/home-page-team');
        break;
    case 'contact_layout':
        get_template_part('/layouts/layouts/contact-layout');
    case 'service_slides':
        get_template_part('/layouts/layouts/service-slides');
        break;
    case 'gallery':
        get_template_part('/layouts/layouts/gallery');
        break;
    case 'service_navigation':
        get_template_part('/layouts/layouts/service-navigation');
        break;
}

