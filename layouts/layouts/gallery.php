<?php
/**
 * Text Block Layout
 *
 */
?>

<?php

get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<div class="d-flex flexible_text row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
    <div class="col-12">
        <?php if (have_rows('gallery_images')) : ?>
            <div class="gallery">
                <div class="row no-gutters">
                    <?php while (have_rows('gallery_images')) : the_row();
                        $image = get_sub_field('image'); //pr($image) ?>
                        <div class="col-6 col-sm-4 col-md-3">
                            <a href="<?php echo $image['url'] ?>" data-lightbox="Our Clinic"
                               data-title="<?php echo $image['alt'] ?>" style="background-image: url(<?php echo $image['sizes']['medium'] ?>)">
                                <!--<img src="<?php /*echo $image['sizes']['medium'] */?>" alt="<?php /*echo $image['alt']; */?>">-->
                            </a>
                        </div>

                    <?php endwhile; ?>
                </div>
            </div>
        <?php endif; ?>


    </div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

get_template_part('/layouts/partials/block-settings-end');

?>
