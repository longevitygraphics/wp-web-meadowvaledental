<?php
/**
 * Text Block Layout
 *
 */
?>

<?php

get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<div class="d-flex flexible_text <?php if ($container == 'container-wide') {
    echo 'no-gutters';
} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
    <div class="col-12">
        <?php
        $counter = 0;
        ?>
        <div class="row">
            <div class="col-lg-6 home-service-items-wrap">
                <ul class="home-service-items">
                    <?php while (have_rows("home_page_services_items")): the_row();
                        $counter++; ?>
                        <li class="<?php echo $counter == 1 ? 'active' : '' ?>" data-num="<?php echo $counter; ?>">
                            <a class="d-lg-none" href="<?php echo get_sub_field('link')['url'] ?>"><?php echo get_sub_field('title'); ?></a><span class="d-none d-lg-inline"><?php echo get_sub_field('title'); ?></span>
                        </li>
                    <?php endwhile; ?>
                </ul>

            </div>
            <div class="col-lg-6 home-service-items-content-wrap">
                <div class="home-service-items-content d-none d-lg-flex" style="background-image: url(<?php echo get_sub_field("service_bg_image")['url'] ?>)">
                    <?php $counter = 0;
                    while (have_rows("home_page_services_items")): the_row();
                        $counter++;
                        $service_link = get_sub_field('link'); ?>
                        <div class="<?php echo $counter == 1 ? 'active' : '' ?>" data-num="<?php echo $counter; ?>">
                            <h3><?php echo get_sub_field('title'); ?></h3>
                            <p><?php echo get_sub_field('para'); ?></p>
                            <br>
                            <?php
                            if ($service_link): ?>
                                <a class="btn btn-outline-white"
                                   href="<?php echo $service_link['url'] ?>"><?php echo $service_link['title'] ?></a>
                            <?php endif; ?>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>


    </div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

get_template_part('/layouts/partials/block-settings-end');

?>
