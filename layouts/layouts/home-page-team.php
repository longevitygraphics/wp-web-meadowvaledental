<?php
/**
 * Text Block Layout
 *
 */
?>

<?php

get_template_part('/layouts/partials/block-settings-start');
$team_page_link = get_sub_field('home_page_team_link');
?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<div class="d-flex flexible_text <?php if ($container == 'container-wide') {
    echo 'no-gutters';
} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
    <div class="col-12">
        <div class="team-top-text">
            <?php
            echo get_sub_field('home_page_team_title');
            ?>
        </div>
        <?php if (have_rows('home_page_team_members')) : ?>
            <div class="team-members">
                <?php while (have_rows('home_page_team_members')) : the_row();
                    $commitment_image = get_sub_field("image"); ?>
                    <a class="item-wrap"
                       href="<?php echo $team_page_link['url'].'/'.get_sub_field("link") ; ?>">
                        <div class="item">
                            <img class="img-fluid" src="<?php echo $commitment_image['url']; ?>"
                                 alt="<?php echo $commitment_image['alt']; ?>">
                            <div class="overlay">
                                <?php echo get_sub_field("para"); ?>
                            </div>
                        </div>
                    </a>
                <?php endwhile; ?>
            </div>
        <?php endif; ?>
        <div class="team-bottom-text text-center">
            <a class="btn btn-outline-primary"
               href="<?php echo $team_page_link['url']; ?>"><?php echo $team_page_link['title']; ?></a>
        </div>
    </div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

get_template_part('/layouts/partials/block-settings-end');

?>
