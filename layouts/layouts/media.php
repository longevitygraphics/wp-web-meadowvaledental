<?php 
/**
 * Media Block Layout
 *
 */
?>

<?php

	include get_template_directory() . '/layouts/partials/block-settings-start.php';

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

	$media_type = get_sub_field('media_type');
	$max_width = get_sub_field('max_width');
	$height = get_sub_field('height');
?>

<div class="d-flex flexible_media <?php if($container == 'container-wide'){echo 'no-gutters';} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">

	<div class="col-12 mx-auto" style="width: 100%; max-width: <?php echo $max_width; ?>; height: <?php echo $height; ?>" <?php if($height != 'auto'): ?>class="variable-height"<?php endif; ?>>
		<?php if($media_type == 'Video'): ?>
			<?php
				$video = get_sub_field('video')['video'];
				include(locate_template('/layouts/components/video.php')); 
			?>
		<?php endif; ?>

		<?php if($media_type == 'Image'): ?>
			<?php
				$image = get_sub_field('image')['image'];
				include(locate_template('/layouts/components/image.php')); 
			?>
		<?php endif; ?>

		<?php if($media_type == 'Image Slider'): ?>
			<?php
				$gallery = get_sub_field('image_slider')['gallery'];
				include(locate_template('/layouts/components/gallery.php')); 
			?>
		<?php endif; ?>		
	</div>

</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php 

	include get_template_directory() . '/layouts/partials/block-settings-end.php';

?>