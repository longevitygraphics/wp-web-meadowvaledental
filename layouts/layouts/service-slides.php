<?php
/**
 * Text Block Layout
 *
 */
?>

<?php

get_template_part('/layouts/partials/block-settings-start');

?>

<!--------------------------------------------------------------------------------------------------------------------------------->

<div class="d-flex flexible_text <?php if ($container == 'container-wide') {
    echo 'no-gutters';
} ?> row <?php the_sub_field('align_items_vertical'); ?> <?php the_sub_field('align_items_horizontal'); ?>">
    <div class="col-12">
        <?php
        $counter = 0;
        $service_slider_image = get_sub_field("service_slide_image");

        ?>
        <?php if (have_rows('service_slide')) : ?>
            <div class="service-slider">
                <div class="service-slider__inner">
                    <?php while (have_rows('service_slide')) : the_row();
                        $slide_image = get_sub_field('image');
                        ?>
                        <div class="service-slider__slide">
                            <div class="row">

                                <div class="col-md-5">

                                    <?php if ($service_slider_image) : ?>
                                        <div class="left" style="background-image: url(<?php echo $service_slider_image['url'] ?>)"></div>
                                    <?php else : ?>
                                        <img src="<?php echo $slide_image['url'] ?>"
                                             alt="<?php echo $slide_image['alt'] ?>"/>
                                    <?php endif; ?>
                                </div>
                                <div class="col-md-7">
                                    <div class="right">
                                        <?php echo get_sub_field('para'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
                <div class="service-slider__arrows">
                    <div class="left"><i class="fas fa-long-arrow-alt-left"></i></div>
                    <div class="right"><i class="fas fa-long-arrow-alt-right"></i></div>
                </div>
            </div>
        <?php endif; ?>

    </div>
</div>

<!--------------------------------------------------------------------------------------------------------------------------------->

<?php

get_template_part('/layouts/partials/block-settings-end');

?>
