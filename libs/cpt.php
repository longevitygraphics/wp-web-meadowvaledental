<?php

function lg_custom_post_type() {
	register_post_type(
		'service',
		array(
			'labels'       => array(
            'name'          => __( 'Services' ),
            'singular_name' => __( 'Service' ),
			),
			'public'       => true,
			'rewrite'      => array(
				'with_front' => false,
			),
			'has_archive'  => false,
			'menu_icon'    => 'dashicons-location',
			'show_in_menu' => 'lg_menu',
			'supports'     => array( 'thumbnail', 'title', 'editor', 'excerpt', 'author' ),
		)
    );
    register_taxonomy(
		'service-tags',
		'service',
		array(
			'hierarchical'  => false,
			'label'         => __( 'Tags', 'wp-web-sunrisedental' ),
			'singular_name' => __( 'Tag', 'wp-web-sunrisedental' ),
			'rewrite'       => true,
			'query_var'     => true,
		)
	);
}

add_action( 'init', 'lg_custom_post_type' );
