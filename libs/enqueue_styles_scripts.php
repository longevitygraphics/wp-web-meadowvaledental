<?php

/* BEGIN CSS FILE */

global $lg_styles, $lg_scripts;

$lg_styles = [
    (object)array(
        "handle" => "lg-fonts",
        "src" => 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800&display=swap',
        "dependencies" => [],
        "version" => false
    ),
    (object)array(
        "handle" => "lg-style-child",
        "src" => get_stylesheet_directory_uri() . "/assets/dist/css/main.css",
        "dependencies" => [],
        "version" => filemtime(get_stylesheet_directory() . "/assets/dist/css/main.css")
    )

];


/* END CSS FILE */

/* BEGIN JS FILE */

$lg_scripts = [
    (object)array(
        "handle" => "lg-script-child",
        "src" => get_stylesheet_directory_uri() . "/assets/dist/js/main.js",
        "dependencies" => array('jquery'),
        "version" => filemtime(get_stylesheet_directory() . "/assets/dist/js/main.js")
    )

];


/* END JS FILE */
function lg_enqueue_styles_scripts()
{
	global $lg_styles, $lg_scripts;
	global $wp;
	$current_url = home_url( $wp->request );

    /* INCLUDE MAIN */

    if ($lg_styles && is_array($lg_styles)) {
        foreach ($lg_styles as $key => $style) {
			//check if page is not /gallery
	        if(!strstr( $current_url, '/gallery') and  $style->handle == 'lightbox2-css'){
				continue;
	        }
	        wp_enqueue_style(
                $style->handle,
                $style->src,
                $style->dependencies,
                $style->version
            );
        }
    }

    if ($lg_scripts && is_array($lg_scripts)) {
        foreach ($lg_scripts as $key => $script) {

	        //check if page is not /gallery
	        if(!strstr( $current_url, '/gallery') and  $script->handle == 'lightbox2-js'){
		        continue;
	        }

            wp_enqueue_script(
                $script->handle,
                $script->src,
                $script->dependencies,
                $script->version,
                true
            );

            wp_enqueue_script($script->handle);
        }
    }

    /* END INCLUDE MAIN */

}

add_action('wp_enqueue_scripts', 'lg_enqueue_styles_scripts');


function lg_enqueue_YTPayer_assets()
{
    global $lg_styles, $lg_scripts;
    //add YTPlayer css file only for home page
    if (is_front_page()) {
        $lg_styles[] = (object)array(
            "handle" => "YTplayer-css",
            "src" => get_stylesheet_directory_uri() . "/assets/vendors/jquery.mb.YTPlayer-master/jquery.mb.YTPlayer.min.css",
            "dependencies" => [],
            "version" => filemtime(get_stylesheet_directory() . "/assets/vendors/jquery.mb.YTPlayer-master/jquery.mb.YTPlayer.min.css")
        );

        //add YTPlayer js only for home page
        $lg_scripts[] = (object)array(
            "handle" => "YTplayer-js",
            "src" => get_stylesheet_directory_uri() . "/assets/vendors/jquery.mb.YTPlayer-master/jquery.mb.YTPlayer.min.js",
            "dependencies" => array('jquery'),
            "version" => filemtime(get_stylesheet_directory() . "/assets/vendors/jquery.mb.YTPlayer-master/jquery.mb.YTPlayer.min.js")
        );
    }
}

//add_action('wp', 'lg_enqueue_YTPayer_assets');

//add lightbox gallery assets to gallery page

function lg_enqueue_lightbox2_assets()
{

    global $lg_styles, $lg_scripts;
    //add lightbox css file
    //if (is_page('our-clinic')) {
        $lg_styles[] = (object)array(
            "handle" => "lightbox2-css",
            "src" => get_stylesheet_directory_uri() . "/assets/vendors/lightbox2-master/dist/css/lightbox.min.css",
            "dependencies" => [],
            "version" => filemtime(get_stylesheet_directory() . "/assets/vendors/lightbox2-master/dist/css/lightbox.min.css")
        );

        //add lightbox js
        $lg_scripts[] = (object)array(
            "handle" => "lightbox2-js",
            "src" => get_stylesheet_directory_uri() . "/assets/vendors/lightbox2-master/dist/js/lightbox.min.js",
            "dependencies" => array('jquery'),
            "version" => filemtime(get_stylesheet_directory() . "/assets/vendors/lightbox2-master/dist/js/lightbox.min.js")
        );
    //}
}

add_action('wp', 'lg_enqueue_lightbox2_assets');


if ($lg_styles && is_array($lg_styles)) {
    foreach ($lg_styles as $key => $style) {
        add_editor_style($style->src);
    }
}


/*Remove wordpress emoji code*/
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('admin_print_styles', 'print_emoji_styles');

/*Remove WP embed*/
function my_deregister_scripts()
{
    wp_deregister_script('wp-embed');
}

add_action('wp_footer', 'my_deregister_scripts');


/*Remove jquery migrate js*/
function remove_jquery_migrate($scripts)
{
    if (!is_admin() && isset($scripts->registered['jquery'])) {
        $script = $scripts->registered['jquery'];

        if ($script->deps) { // Check whether the script has any dependencies
            $script->deps = array_diff($script->deps, array(
                'jquery-migrate'
            ));
        }
    }
}

add_action('wp_default_scripts', 'remove_jquery_migrate');


//Remove Gutenburg css
add_action('wp_print_styles', 'wps_deregister_gutenberg_styles', 100);
function wps_deregister_gutenberg_styles()
{
    wp_dequeue_style('wp-block-library');
}
