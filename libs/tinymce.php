<?php

global $lg_tinymce_custom;
$selector          = 'h1,h2,h3,h4,h5,h6';
$lg_tinymce_custom = array(
	'title' => 'Custom',
	'items' => array(
		array(
			'title'    => 'Heading with line',
			'selector' => $selector,
			'classes'  => 'h-with-line',
		),
		array(
			'title'    => 'Heading with Sunrise Icon',
			'selector' => $selector,
			'classes'  => 'h-with-icon',
		),
		array(
			'title'    => 'Remove list padding',
			'selector' => 'ul',
			'classes'  => 'remove-list-padding',
		),
		array(
			'title' => 'Heading with Icon on left',
			'items' => array(
				array(
					'title'    => 'Sun',
					'selector' => $selector,
					'classes'  => 'h-with-sun',
				),
				array(
					'title'    => 'Camera',
					'selector' => $selector,
					'classes'  => 'h-with-camera',
				),
				array(
					'title'    => 'Arrow Up',
					'selector' => $selector,
					'classes'  => 'h-with-arrow-up',
				),
			),
        ),
        array(
			'title'    => 'Open Book Modal Button',
			'selector' => 'a',
			'classes'  => 'btn-open-modal',
		),
	),
);
