<div class="top-banner <?php if ( is_front_page() ) {
	echo 'home-top-banner';} ?>">
	<?php if ( have_rows( 'top_banner' ) ) : ?>
		<div class='banner-slider lg-gallery'>
		<?php
		while ( have_rows( 'top_banner' ) ) :
			the_row();
			?>
			<?php
			$image        = get_sub_field( 'image' );
			$mobile_image = get_sub_field( 'mobile_image' );
			$content      = get_sub_field( 'content' );
			?>
		<div class="single-slide">
			<picture>
				<source 
					media="(max-width: 480px)" 
					srcset="
					<?php
					if ( $mobile_image ) {
						echo wp_get_attachment_image_src( $mobile_image['ID'], 'mobile-banner' )[0];
					} else {
						echo wp_get_attachment_image_src( $image['ID'], 'mobile-banner' )[0];
					}
					?>
					">
				<img class="img-full" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
			</picture>
			<?php if ( $content || $form ) : ?>
			<div class="top-banner-overlay">
				<div class="banner-text container d-flex justify-content-between align-items-center">
					<div>
						<?php echo $content; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div>
		<?php endwhile; // end of while have rows ?>
		</div> 
	<?php endif; // end of if have rows ?>
	<?php if( is_front_page() ) : ?>
	<div class='top-banner-form'>
		<?php echo do_shortcode( get_field( 'top_banner_form' ) ); ?>
	</div>
	<?php endif; ?>
</div>
